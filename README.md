
See original docs at: https://github.com/jupyter/docker-stacks/tree/master/datascience-notebook

To run locally, listening on port 8888:
```bash
docker run --rm -ti -v $(pwd):/home/jovyan -p 127.0.0.1:8888:8888 mmonga/statslab start-notebook.sh --NotebookApp.token=''
```

A useful script when you want several instances:

```bash
#!/bin/bash

ALERT='\033[0;35m'
( sleep 10; echo -e "$ALERT Local address: $(docker port statslab 8888)";
  x-www-browser http://$(docker port bebras-rubrics 8888) ) &
docker run --rm -ti -p 127.0.0.1:0:8888 \
       -v $(pwd):/home/jovyan/work  --name statslab jupyter/scipy-notebook start-notebook.sh --NotebookApp.token='' $*

```
